﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TaskbarClock;

namespace NETProjectTutorial
{
    public partial class Frmfiltro : Form
    {
        private DataSet dsSistema;
        private BindingSource bsProductoFactura;
        private DataSet dsEmpleado;
        private BindingSource bsEmpelado;

        public Frmfiltro()
        {
            InitializeComponent();
            bsProductoFactura = new BindingSource();
        }

        public DataSet DsSistema
        {
            set
            {
                dsSistema = value;
            }
        }

        private void cmbFiltre_SelectedIndexChanged(object sender, EventArgs e)
        {
            _needUpdate = true;
        }

        private bool _canUpdate = true;
        private bool _needUpdate = false;

        private void UpdateData()
        {
            if (cmbFiltre.Text.Length > 1)
            {

                List<Empleado> searchData = dsSistema.Tables["Empleado"].AsEnumerable().Select(
                    dataRow =>
                    new Empleado
                    {
                        Id = dataRow.Field<Int32>("Id"),
                        Nombre = dataRow.Field<String>("Nombres"),
                        Apellidos = dataRow.Field<String>("Apellidos")
                    }).ToList();

                HandleTextChanged(searchData.FindAll(e => e.Nombre.Contains(cmbFiltre.Text)));
            }
            else
            {
                RestartTimer();
            }
        }

        private void HandleTextChanged(List<Empleado> dataSource)
        {
            var text = cmbFiltre.Text;

            if (dataSource.Count() > 0)
            {
                cmbFiltre.DataSource = dataSource;

                var sText = cmbFiltre.Items[0].ToString();
                cmbFiltre.SelectionStart = text.Length;
                cmbFiltre.SelectionLength = sText.Length - text.Length;
                cmbFiltre.DroppedDown = true;
                return;
            }
            else
            {
                cmbFiltre.DroppedDown = false;
                cmbFiltre.SelectionStart = text.Length;
            }
        }

        private void RestartTimer()
        {
            timer1.Stop();
            _canUpdate = false;
            Time.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            _canUpdate = true;
            timer1.Stop();
            UpdateData();

        }
    }
}
